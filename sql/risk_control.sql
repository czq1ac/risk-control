/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MariaDB
 Source Server Version : 100225
 Source Host           : localhost:3306
 Source Schema         : risk_control

 Target Server Type    : MariaDB
 Target Server Version : 100225
 File Encoding         : 65001

 Date: 13/09/2021 00:24:37
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for abstraction_vo
-- ----------------------------
DROP TABLE IF EXISTS `abstraction_vo`;
CREATE TABLE `abstraction_vo`  (
  `object_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'Abstraction 名称',
  `label` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `model_id` bigint(20) NOT NULL COMMENT 'MODEL ID',
  `aggregate_type` int(11) NOT NULL COMMENT '统计类型',
  `search_field` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `search_interval_type` int(11) NOT NULL COMMENT '时间段类型',
  `search_interval_value` int(11) NOT NULL COMMENT '时间段具体值',
  `function_field` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `rule_script` varchar(2048) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '数据校验',
  `rule_definition` varchar(2048) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `status` int(11) NOT NULL COMMENT '状态',
  `comment` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '备注',
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`object_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1436224319805050881 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of abstraction_vo
-- ----------------------------

-- ----------------------------
-- Table structure for activation_vo
-- ----------------------------
DROP TABLE IF EXISTS `activation_vo`;
CREATE TABLE `activation_vo`  (
  `object_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `ACTIVATION_NAME` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '名称',
  `LABEL` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `MODEL_ID` bigint(11) NOT NULL COMMENT 'model id',
  `COMMENT` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '注释',
  `BOTTOM` int(11) NULL DEFAULT 0 COMMENT '底部阀值',
  `MEDIAN` int(11) NOT NULL DEFAULT 0 COMMENT '中间阀值',
  `HIGH` int(11) NOT NULL DEFAULT 0 COMMENT '顶部阀值',
  `STATUS` int(11) NOT NULL COMMENT '状态',
  `RULE_ORDER` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `CREATE_TIME` datetime(0) NULL DEFAULT NULL,
  `UPDATE_TIME` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`object_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1435242820565774337 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of activation_vo
-- ----------------------------

-- ----------------------------
-- Table structure for field_vo
-- ----------------------------
DROP TABLE IF EXISTS `field_vo`;
CREATE TABLE `field_vo`  (
  `object_id` bigint(20) NOT NULL,
  `model_id` bigint(20) NULL DEFAULT NULL,
  `field_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `label` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `field_type` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `validate_type` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `validate_args` varchar(225) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `indexed` tinyint(1) NULL DEFAULT NULL,
  `must_field` tinyint(1) NULL DEFAULT NULL,
  PRIMARY KEY (`object_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of field_vo
-- ----------------------------
INSERT INTO `field_vo` VALUES (1437089566866554882, 1437089465972572162, 'entry', '实体', 'long', '2021-09-13 00:23:47', NULL, NULL, NULL, 1, 1);
INSERT INTO `field_vo` VALUES (1437089566866554883, 1437089465972572162, 'event', '事件', 'long', '2021-09-13 00:23:47', NULL, NULL, NULL, 1, 1);
INSERT INTO `field_vo` VALUES (1437089566883332098, 1437089465972572162, 'date', '时间', 'long', '2021-09-13 00:23:47', NULL, NULL, NULL, 1, 1);

-- ----------------------------
-- Table structure for model_vo
-- ----------------------------
DROP TABLE IF EXISTS `model_vo`;
CREATE TABLE `model_vo`  (
  `object_id` bigint(20) NOT NULL,
  `guid` bigint(20) NULL DEFAULT NULL,
  `model_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `label` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `entity_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `entry_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `reference_date` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` int(1) NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `field_validate` tinyint(1) NULL DEFAULT NULL,
  `code` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `template` tinyint(1) NULL DEFAULT NULL,
  PRIMARY KEY (`object_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of model_vo
-- ----------------------------
INSERT INTO `model_vo` VALUES (1437089465972572162, 1437089465959989248, NULL, '测试模型', 'entry', 'event', 'date', 1, '2021-09-13 00:23:23', '2021-09-13 00:23:47', NULL, NULL, 0);

-- ----------------------------
-- Table structure for pre_item_vo
-- ----------------------------
DROP TABLE IF EXISTS `pre_item_vo`;
CREATE TABLE `pre_item_vo`  (
  `object_id` bigint(20) NOT NULL,
  `model_id` bigint(20) NULL DEFAULT NULL,
  `source_field` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `source_label` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `dest_field` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `label` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `plugin` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `req_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `config_json` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` int(1) NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `args` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`object_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of pre_item_vo
-- ----------------------------

-- ----------------------------
-- Table structure for rule_vo
-- ----------------------------
DROP TABLE IF EXISTS `rule_vo`;
CREATE TABLE `rule_vo`  (
  `object_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `MODEL_ID` bigint(20) NOT NULL COMMENT '模型ID',
  `ACTIVATION_ID` bigint(20) NOT NULL COMMENT '激活ID',
  `NAME` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `LABEL` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '规则名称',
  `SCRIPTS` varchar(2048) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '检验脚本',
  `INIT_SCORE` int(11) NOT NULL DEFAULT 0 COMMENT '初始分数',
  `BASE_NUM` int(11) NOT NULL DEFAULT 0 COMMENT '基数',
  `OPERATOR` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '运算符',
  `ABSTRACTION_NAME` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '抽象名称',
  `RATE` int(11) NOT NULL DEFAULT 100 COMMENT '比例',
  `MAX` int(11) NOT NULL DEFAULT 0 COMMENT '最大得分值',
  `STATUS` int(11) NOT NULL COMMENT '状态',
  `RULE_DEFINITION` varchar(2048) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `CREATE_TIME` datetime(0) NULL DEFAULT NULL,
  `UPDATE_TIME` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`object_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1435936273650012161 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of rule_vo
-- ----------------------------

-- ----------------------------
-- Table structure for sys_account
-- ----------------------------
DROP TABLE IF EXISTS `sys_account`;
CREATE TABLE `sys_account`  (
  `object_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `enabled` tinyint(1) NULL DEFAULT NULL,
  `locked` tinyint(1) NULL DEFAULT NULL,
  `user_id` bigint(20) NULL DEFAULT NULL,
  `alias` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`object_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_account
-- ----------------------------
INSERT INTO `sys_account` VALUES (1, 'root', '$2a$10$RMuFXGQ5AtH4wOvkUqyvuecpqUSeoxZYqilXzbz50dceRsga.WYiq', 1, 0, 1000, NULL);
INSERT INTO `sys_account` VALUES (2, 'admin', '$2a$10$RMuFXGQ5AtH4wOvkUqyvuecpqUSeoxZYqilXzbz50dceRsga.WYiq', 1, 0, 2000, NULL);

-- ----------------------------
-- Table structure for sys_code
-- ----------------------------
DROP TABLE IF EXISTS `sys_code`;
CREATE TABLE `sys_code`  (
  `object_id` bigint(20) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `tag` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `order_no` int(11) NULL DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `deleted` int(11) NULL DEFAULT NULL,
  `created_date` datetime(0) NULL DEFAULT NULL,
  `last_update_date` datetime(0) NULL DEFAULT NULL,
  `created_by` bigint(20) NULL DEFAULT NULL,
  `last_update_by` bigint(20) NULL DEFAULT NULL,
  `parent_id` bigint(20) NULL DEFAULT NULL,
  PRIMARY KEY (`object_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_code
-- ----------------------------

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `object_id` bigint(11) NOT NULL AUTO_INCREMENT,
  `deleted` int(11) NULL DEFAULT NULL,
  `name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `tag` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `description` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `created_date` datetime(0) NULL DEFAULT NULL,
  `created_by` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `last_update_date` datetime(0) NULL DEFAULT NULL,
  `last_update_by` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`object_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1348535667601870851 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1348535595237543938, 0, '管理员', 'admin', NULL, '2021-01-11 15:41:55', '1000', NULL, NULL);
INSERT INTO `sys_role` VALUES (1348535667601870850, 0, '普通用户', 'user', NULL, '2021-01-11 15:42:12', '1000', NULL, NULL);

SET FOREIGN_KEY_CHECKS = 1;
