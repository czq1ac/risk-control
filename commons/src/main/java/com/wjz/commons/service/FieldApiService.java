package com.wjz.commons.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wjz.commons.entity.bo.AjaxResult;
import com.wjz.commons.entity.vo.FieldVo;
import com.wjz.commons.mapper.FieldVoMapper;
import com.wjz.commons.util.ResultTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author wjz
 * @date 2021/7/9 17:18
 */
@Service
public class FieldApiService extends ServiceImpl<FieldVoMapper, FieldVo> {

    @Autowired
    private FieldVoMapper fieldVoMapper;

    public void deleteByModelId(Long modelId) {
        fieldVoMapper.delete(new LambdaQueryWrapper<FieldVo>().eq(FieldVo::getModelId, modelId));
    }

    public AjaxResult deleteById(Long objectId) {
        LambdaQueryWrapper<FieldVo> wrapper = new LambdaQueryWrapper<FieldVo>()
                .eq(FieldVo::getObjectId, objectId).ne(FieldVo::getMustField, true);
        Integer count = fieldVoMapper.selectCount(wrapper);
        if (count == 0) {
            return ResultTool.fail("不能删除必备字段！");
        }
        fieldVoMapper.delete(wrapper);
        return ResultTool.success();
    }
}
