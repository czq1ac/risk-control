package com.wjz.commons.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wjz.commons.entity.vo.AbstractionVo;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author wjz
 * @date 2021/9/5 21:56
 */
@Mapper
public interface AbstractionVoMapper extends BaseMapper<AbstractionVo> {
}
