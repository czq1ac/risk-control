package com.wjz.commons.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wjz.commons.entity.vo.PreItemVo;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author wjz
 * @date 2021/7/9 17:21
 */
@Mapper
public interface PreItemVoMapper extends BaseMapper<PreItemVo> {
}
