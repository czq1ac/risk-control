package com.wjz.commons.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wjz.commons.entity.vo.SysRole;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author wjz
 * @date 2020/11/29 22:14
 * @Description
 */
@Mapper
public interface SysRoleMapper extends BaseMapper<SysRole> {
}
