package com.wjz.commons.entity.bo;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author wjz
 * @date 2021/7/7 16:35
 */
@Data
@EqualsAndHashCode
public class BasePage {

    /**
     * 第几页
     */
    private Integer current = 1;

    /**
     * 每页显示数   默认10条
     */
    private Integer pageSize = 10;
}
