package com.wjz.commons.entity.vo;

import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * @author wjz
 * @date 2021/7/9 16:40
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "FieldVo", description = "字段实体")
public class FieldVo implements Serializable {

    @TableId
    private Long objectId;

    private Long modelId;

    private String fieldName;

    private String label;

    private String fieldType;

    private Date createTime;

    private Date updateTime;

    private String validateType;

    private String validateArgs;

    private Boolean indexed;

    private Boolean mustField;
}
