import {merge} from './commonStore'
import account from './account'
import setting from './setting'
import loading from './loading'
import modelManage from './systemManage/modelManage'
import fieldManage from './systemManage/fieldManage'
import pretreatment from './systemManage/pretreatment'
import abstractionList from './systemManage/abstractionList'
import activation from './systemManage/activation'
import rule from './systemManage/rule'
//导入点


export default merge({
  account,
  setting,
  modelManage,
  loading,
  fieldManage,
  pretreatment,
  abstractionList,
  activation,
  rule,
//插入点
})
