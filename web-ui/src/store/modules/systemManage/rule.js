import {addRule, deleteById, disable, queryAllRule, update} from '@/services/systemManage/rule'
import {complexParse, storePrompt} from '@/store/modules/commonStore'

export default {
  namespaced: true,
  state: {
    tableData: [],
    showTableDada: [],
    currentKey: undefined,
    currentActivationKey: undefined,
    rulePopupVisible: false,
    rulePopupTitle: undefined,
    currentEditKey: undefined,
    editData: null
  },
  getters: {
    generateScript() {
      return function (obj) {
        let script = 'class AbstractionCheckScript {\n' +
          '  public boolean check(def data, def lists) {    if (' + complexParse(obj) + ')\n' +
          '        return true;\n' +
          '    else\n' +
          '        return false;\n' +
          '}}'
        return script
      }
    }
  },
  actions: {
    async queryAllRule({commit, state}, payload) {
      await queryAllRule({
        ...payload,
        modelId: state.currentKey,
        activationId: state.currentActivationKey
      }).then((obj) => {
        let {data, success} = obj.data
        if (success) {
          commit("setState", {name: 'tableData', value: data})
          commit("setState", {name: 'showTableDada', value: data})
        }
      })
    },
    async deleteById({commit, state, dispatch}, payload) {
      await deleteById(payload).then((obj) => {
        storePrompt(obj.data)
        dispatch('queryAllRule')
      })
    },
    async disable({commit, state, dispatch}, payload) {
      await disable(payload).then(() => {
        dispatch('queryAllRule')
      })
    },
    async update({commit, state}, payload) {
      await update({
        ...payload,
        objectId: state.currentEditKey,
        modelId: state.currentKey,
        activationId: state.currentActivationKey
      }).then((obj) => {
        storePrompt(obj.data)
        commit("setState", {name: 'rulePopupVisible', value: false})
      })
    },
    async addRule({commit, state}, payload) {
      await addRule({...payload, modelId: state.currentKey, activationId: state.currentActivationKey}).then((obj) => {
        storePrompt(obj.data)
        commit("setState", {name: 'rulePopupVisible', value: false})
        commit("setState", {name: 'rulePopupTitle', value: undefined})
      })
    },
  }
}
