import {addAbstraction, deleteById, queryAllAbstraction, update} from '@/services/systemManage/abstractionList'
import {storePrompt} from '@/store/modules/commonStore'

export default {
  namespaced: true,
  state: {
    tableData: [],
    showTableDada: [],
    currentKey: undefined,
    popupVisible: false,
    popupTitle: undefined,
    currentEditKey: undefined,
    editData: null
  },
  actions: {
    async queryAllAbstraction({commit, state}, payload) {
      await queryAllAbstraction({...payload, modelId: state.currentKey}).then((obj) => {
        let {data, success} = obj.data
        if (success) {
          commit("setState", {name: 'tableData', value: data})
          commit("setState", {name: 'showTableDada', value: data})
        }
      })
    },
    async deleteById({commit, state,dispatch}, payload) {
      await deleteById(payload).then((obj) => {
        storePrompt(obj.data)
        dispatch('queryAllAbstraction')
      })
    },
    async update({commit, state}, payload) {
      await update({...payload, objectId: state.currentEditKey, modelId: state.currentKey}).then((obj) => {
        storePrompt(obj.data)
        commit("setState", {name: 'popupVisible', value: false})
      })
    },
    async addAbstraction({commit, state}, payload) {
      await addAbstraction({...payload, modelId: state.currentKey}).then((obj) => {
        storePrompt(obj.data)
        commit("setState", {name: 'popupVisible', value: false})
        commit("setState", {name: 'popupTitle', value: undefined})
      })
    },
  }
}
