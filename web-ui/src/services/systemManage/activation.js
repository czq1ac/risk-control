import {BASE_URL} from '@/services/api'
import {METHOD, request} from '@/utils/request'

export async function queryAllActivation(payload) {
  return request(`${BASE_URL}/activation`, METHOD.GET, payload)
}

export async function disable(payload) {
  return request(`${BASE_URL}/activation/disable`, METHOD.GET, payload)
}

export async function addActivation(payload) {
  return request(`${BASE_URL}/activation`, METHOD.POST, payload)
}

export async function deleteById(payload) {
  return request(`${BASE_URL}/activation/deleteById/${payload}`, METHOD.DELETE)
}

export async function update(payload) {
  return request(`${BASE_URL}/activation`, METHOD.PUT, payload)
}
