package com.wjz.admin.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.wjz.commons.entity.bo.AjaxResult;
import com.wjz.commons.entity.vo.FieldVo;
import com.wjz.commons.service.FieldApiService;
import com.wjz.commons.util.ResultTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

/**
 * @author wjz
 * @date 2021/7/9 17:17
 */
@RestController
@RequestMapping("/field")
public class FieldApiController {

    @Autowired
    private FieldApiService fieldApiService;

    @PostMapping("/saveBatch")
    public AjaxResult saveBatch(@RequestBody List<FieldVo> list) {
        for (FieldVo fieldVo : list) {
            fieldVo.setCreateTime(new Date());
        }
        return ResultTool.success(fieldApiService.saveBatch(list));
    }

    @PostMapping
    public AjaxResult add(@RequestBody FieldVo fieldVo) {
        return ResultTool.success(fieldApiService.save(fieldVo.setMustField(false).setCreateTime(new Date())));
    }

    @DeleteMapping("/deleteById/{objectId}")
    public AjaxResult deleteById(@PathVariable Long objectId) {
        return fieldApiService.deleteById(objectId);
    }

    @GetMapping
    public AjaxResult queryAllField(Long modelId) {
        return ResultTool.success(fieldApiService.list(new LambdaQueryWrapper<FieldVo>().eq(FieldVo::getModelId, modelId)));
    }

    @PutMapping
    public AjaxResult update(@RequestBody FieldVo fieldVo) {
        return ResultTool.success(fieldApiService.updateById(fieldVo.setUpdateTime(new Date())));
    }
}
