package com.wjz.admin.controller;

import cn.hutool.core.util.IdUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.wjz.commons.entity.bo.AjaxResult;
import com.wjz.commons.entity.vo.AbstractionVo;
import com.wjz.commons.enums.MasterConstants;
import com.wjz.commons.service.AbstractionApiService;
import com.wjz.commons.util.ResultTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

/**
 * @author wjz
 * @date 2021/9/5 21:59
 */
@RestController
@RequestMapping("/abstraction")
public class AbstractionApiController {

    @Autowired
    private AbstractionApiService abstractionApiService;

    @GetMapping
    public AjaxResult queryAllPreItem(Long modelId) {
        return ResultTool.success(abstractionApiService.list(new LambdaQueryWrapper<AbstractionVo>().eq(AbstractionVo::getModelId, modelId)));
    }

    @PostMapping
    public AjaxResult save(@RequestBody AbstractionVo abstractionVo) {
        Long objectId = IdUtil.getSnowflake().nextId();
        abstractionVo.setObjectId(objectId)
                .setName("abstraction_" + objectId)
                .setStatus(MasterConstants.NEW);
        return ResultTool.success(abstractionApiService.save(abstractionVo.setCreateTime(new Date())));
    }

    @PutMapping
    public AjaxResult update(@RequestBody AbstractionVo abstractionVo) {
        return ResultTool.success(abstractionApiService.updateById(abstractionVo.setUpdateTime(new Date())));
    }

    @DeleteMapping("/deleteById/{objectId}")
    public AjaxResult deleteById(@PathVariable Long objectId) {
        return ResultTool.success(abstractionApiService.removeById(objectId));
    }
}
